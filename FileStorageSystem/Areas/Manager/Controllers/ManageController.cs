﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FileStorageSystem.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System.Threading.Tasks;
using System.Net.Mail;
using FileStorageSystem.ViewModels;
using System.Web.Configuration;
using FileStorageSystem.Common;

namespace FileStorageSystem.Areas.Manager.Controllers
{
    public class ManageController : Controller
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        // GET: Manager/Manage
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Get_items(string fileSystemId, string itemStatus, [DataSourceRequest] DataSourceRequest request)
        {
            FileStorageSystemContext context = new FileStorageSystemContext();

            Guid id;
            Guid.TryParse(fileSystemId, out id);

            IEnumerable<FileSystemItem> items = Enumerable.Empty<FileSystemItem>();

            if ((itemStatus == null) || (itemStatus == string.Empty))
            {
                items = from storageItem in context.FileSystemItems
                        where storageItem.TypeOfSystemItemID == 2
                        orderby storageItem.TypeOfSystemItemID
                        select storageItem;
            }
            else
            {
                int status = int.Parse(itemStatus);
                items = from storageItem in context.FileSystemItems
                        where storageItem.TypeOfSystemItemID == 2 && storageItem.isApproved == status
                        orderby storageItem.TypeOfSystemItemID
                        select storageItem;
            }
            List<FileItemViewModel> files = new List<FileItemViewModel>();

            foreach (var item in items)
            {
                files.Add(new FileItemViewModel()
                {
                    FileSystemItemID = item.FileSystemItemID,
                    CreatingDate = item.CreatingDate,
                    isApproved = item.isApproved,
                    IsSharedToAll = item.IsSharedToAll,
                    ItemDescription = item.ItemDescription,
                    ItemName = item.ItemName,
                    ParentFolderID = item.ParentFolderID,
                    TypeOfSystemItemID = item.TypeOfSystemItemID,
                    OwnerName = context.Users.First(u => u.UserID == item.OwnerID).UserName,
                    FileExtensionId = item.FileExtensionId,
                    AreaName = context.Areas.First(a => a.AreaId == item.AreaId).AreaName,
                    RegionName = context.Regions.First(a => a.RegionId == item.RegionId).RegionName,
                });
            }

            return Json(files.ToDataSourceResult(request));
        }

        public ActionResult Download(Guid id)
        {
            var context = new FileStorageSystemContext();
            var filePath = context.FileSystemItems
                 .Where(e => e.FileSystemItemID.Equals(id))
                 .Select(s => s.PhysicalPath).FirstOrDefault();
            var fileName = context.FileSystemItems
                 .Where(e => e.FileSystemItemID.Equals(id))
                 .Select(s => s.ItemName).FirstOrDefault();
            var fileExtension = context.FileSystemItems
                 .Where(e => e.FileSystemItemID.Equals(id))
                 .Select(s => s.FileExtensionId).FirstOrDefault();

            var fullName = fileName + "." + fileExtension;

            DownloadFile.TransmitFile(filePath, fullName, Response);

            return Content("");
        }

        public ActionResult ApproveItem(Guid id)
        {
            using (var context = new FileStorageSystemContext())
            {
                var oldItem = context.FileSystemItems.Find(id);
                if (oldItem != null)
                {
                    oldItem.isApproved = 1;
                    var scresult = context.SaveChanges();
                    if (scresult == 1)
                    {
                        User user = new FileStorageSystemContext().Users.First(u => u.UserID == oldItem.OwnerID);
                        Task.Factory.StartNew(() => MessageService.SendMailToUser(oldItem, TypeOfAction.APPROVED, GetMainUrl()));
                    }
                }
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult RejectItem(Guid id)
        {
            using (var context = new FileStorageSystemContext())
            {
                var oldItem = context.FileSystemItems.Find(id);
                if (oldItem != null)
                {
                    oldItem.isApproved = 0;
                    var scresult = context.SaveChanges();
                    if (scresult == 1)
                    {
                        User user = new FileStorageSystemContext().Users.First(u => u.UserID == oldItem.OwnerID);
                        Task.Factory.StartNew(() => MessageService.SendMailToUser(oldItem, TypeOfAction.REJECTED, GetMainUrl()));
                    }
                }
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }

        private string GetMainUrl()
        {
            var tempUrl = Request.Url;
            string url = tempUrl.Authority + string.Join(null, tempUrl.Segments, 0, (tempUrl.Segments.Length - 3));
            return url;
        }


        public ActionResult DeleteItem(Guid itemId)
        {
            using (FileStorageSystemContext context = new FileStorageSystemContext())
            {
                foreach (SharedFile file in context.SharedFiles.Where(f => f.FileId == itemId).ToList())
                {
                    context.SharedFiles.Remove(file);
                    context.SaveChanges();
                }

            }
            FileSystemItem deletedFile;
            using (FileStorageSystemContext context = new FileStorageSystemContext())
            {
                deletedFile = context.FileSystemItems.First(x => x.FileSystemItemID == itemId);
                FileSystemItem temp = new FileSystemItem() { ItemName = deletedFile.ItemName, ItemDescription = deletedFile.ItemDescription, OwnerID = deletedFile.OwnerID };
                context.FileSystemItems.Remove(deletedFile);
                context.SaveChanges();
                Task.Factory.StartNew(() => MessageService.SendMailToUser(temp, TypeOfAction.DELETED, GetMainUrl()));

            }


            return Json("", JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult ApproveUpload(string id)
        {
            Guid fileid = Guid.Parse(id);
            FileSystemItem file = new FileStorageSystemContext().FileSystemItems.First(f => f.FileSystemItemID == fileid);
            List<FileItemViewModel> files = new List<FileItemViewModel>();
            FileStorageSystemContext context = new FileStorageSystemContext();
            var fileItem = new FileItemViewModel()
            {
                FileSystemItemID = file.FileSystemItemID,
                CreatingDate = file.CreatingDate,
                isApproved = file.isApproved,
                IsSharedToAll = file.IsSharedToAll,
                ItemDescription = file.ItemDescription,
                ItemName = file.ItemName,
                ParentFolderID = file.ParentFolderID,
                TypeOfSystemItemID = file.TypeOfSystemItemID,
                OwnerName = context.Users.First(u => u.UserID == file.OwnerID).UserName,
                FileExtensionId = file.FileExtensionId,
                AreaName = context.Areas.First(s => s.AreaId == file.AreaId).AreaName,
                RegionName = context.Regions.First(r => r.RegionId == file.RegionId).RegionName
            };

            return PartialView("~/Areas/Manager/Views/Manage/ApproveUploadView.cshtml", fileItem);
        }

    }
}