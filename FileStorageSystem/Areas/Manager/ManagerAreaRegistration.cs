﻿using System.Web.Mvc;

namespace FileStorageSystem.Areas.Manager
{
    public class ManagerAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Manager";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Manager_default",
                "Manager/{controller}/{action}/{id}",
                new { area = "Manager", controller = "Manage", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}