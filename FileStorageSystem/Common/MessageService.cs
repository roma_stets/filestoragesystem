﻿using FileStorageSystem.Models;
using FileStorageSystem.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;

namespace FileStorageSystem.Common
{
    public static class MessageService
    {
        private static string From = WebConfigurationManager.AppSettings["From"].ToString();
        private static string[] adminsEmails = WebConfigurationManager.AppSettings["adminsEmails"].Split(',');
        private static SmtpClient client = new SmtpClient();
        
        public static Task SendEmailAsync(string email,string subject, string message)
        {
            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(From);
            msg.To.Add(email);
            msg.Subject = subject;
            msg.IsBodyHtml = true;
            msg.Body = message;
            client.Send(msg);

            return Task.FromResult(0);
        }

        public static Task SendToAdminsApproveEmail(FileSystemItem fileItem, string url)
        {
            MailMessage msg = new MailMessage();
            msg.From= new MailAddress(From);
            foreach (string email in adminsEmails)
            {
                msg.To.Add(email);
            }           
            string userName = new FileStorageSystemContext().Users.Where(u => u.UserID == fileItem.OwnerID).First().UserName;

            msg.Subject = "Request to upload file ";
            msg.IsBodyHtml = true;
            msg.Body = string.Format(requestApproveMsg, userName,fileItem.ItemName,fileItem.ItemDescription,url + "/Manager/Manage/ApproveUpload/" + fileItem.FileSystemItemID);
            client.Send(msg);
            return Task.FromResult(0);
        }

        public static Task SendMailToUser(FileSystemItem fileItem, TypeOfAction action,string url)
        {
            
            User user = new FileStorageSystemContext().Users.First(u => u.UserID == fileItem.OwnerID);
            //var tempUrl = Request.Url;
            //string url = tempUrl.Authority + string.Join(null, tempUrl.Segments, 0, (tempUrl.Segments.Length - 3));
                      
            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(From);
            msg.To.Add(user.UserEmail);
            msg.IsBodyHtml = true;
            switch (action)
            {
                case TypeOfAction.APPROVED:                    
                    msg.Subject = "Upload file was approved";
                    msg.Body = string.Format(approvedMsg, fileItem.ItemName, fileItem.ItemDescription, url);
                    break;
                case TypeOfAction.REJECTED:
                    msg.Body = string.Format(rejectedMsg, fileItem.ItemName, fileItem.ItemDescription,url);                    
                    msg.Subject = "Upload file was rejected"; break;
                case TypeOfAction.DELETED:
                    msg.Body=string.Format(deletedMsg, fileItem.ItemName, fileItem.ItemDescription,url);                    
                    msg.Subject = "Upload file was deleted"; break;
            }
            
            client.Send(msg);
            return Task.FromResult(0);
        }

        private static string requestApproveMsg = "<h1>The user {0} send request to approve uppload file: </h1> " +
                "<p><h2>File name: {1} </h2></p>" +
                "<p><h2>File description: {2}</h2></p><br>" +
                "<h2>Link to: <a href='http://{3}' >file</a> </h2>";
        private static string approvedMsg = "<h1>Congratulations!!! Your file: </h1> " +
                 "<p><h2>File name: {0} </h2></p>" +
                 "<p><h2>File description: {1}</h2></p><br>" +
                 "<p><h1>Was approved !!!!!</h1></p>  " +
                 "<h2>Link to: <a href='http://{2}'>Home page</a> </h2>";

        private static string rejectedMsg = "<h1> Unfortunately your file: </h1> " +
                   "<p><h2>File name: {0}</h2></p>" +
                   "<p><h2>File description: {1}</h2></p><br>" +
                   "<p><h1>Was rejected !!!!!</h1></p>  " +
                   "<h2>Link to: <a href='http://{2}'>Home page</a> </h2>";
        private static string deletedMsg = "<h1> Unfortunately your file: </h1> " +
                    "<p><h2>File name: {0}</h2></p>" +
                    "<p><h2>File description: {1}</h2></p><br>" +
                    "<p><h1>Was deleted !!!!!</h1></p>  " +
                    "<h2>Link to: <a href='http://{2}'>Home page</a> </h2>";
    }
}