﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FileStorageSystem.ViewModels
{
    public class TabNavViewModel
    {
        public int AreaIndex { get; set; }
        public int RegionIndex { get; set; }
    }
}