﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FileStorageSystem.ViewModels
{
    public enum TypeOfAction
    {
        DELETED=1,
        APPROVED=2,
        REJECTED=3
    }
}