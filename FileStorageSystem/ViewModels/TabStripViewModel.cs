﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FileStorageSystem.ViewModels
{
    public class TabStripViewModel
    {
        public string text { get; set; }
        public string content { get; set; }
        public string imageUrl { get; set; }
    }
}