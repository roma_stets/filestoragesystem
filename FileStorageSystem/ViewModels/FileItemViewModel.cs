﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FileStorageSystem.ViewModels
{
	public class FileItemViewModel
	{
		public System.Guid FileSystemItemID { get; set; }
		[Required]
		public string ItemName { get; set; }
		public string ItemDescription { get; set; }
		public System.DateTime CreatingDate { get; set; }
		public Nullable<System.Guid> ParentFolderID { get; set; }
		public int TypeOfSystemItemID { get; set; }
		public int IsSharedToAll { get; set; }
		public string FileExtensionId { get; set; }
		public bool IsSharedToAllBool
		{
			get { return IsSharedToAll == 1 ? true : false; }
			set { IsSharedToAll = value == true ? 1 : 0; }
		}
		public int isApproved { get; set; }
		public string OwnerName { get; set; }
		public int isEditable { get; set; }
		public int AreaId { get; set; }
		public string AreaName { get; set; }
		public string RegionName { get; set; }
		public int RegionId { get; set; }

	}
}