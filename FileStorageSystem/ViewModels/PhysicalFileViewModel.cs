﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FileStorageSystem.ViewModels
{
    public class PhysicalFileViewModel
    {
        public string PhysicalPath { get; set; }
        public Guid fileId { get; set; }
        public string FileExtension { get; set; }
        public double FileSize { get; set; }
    }
}