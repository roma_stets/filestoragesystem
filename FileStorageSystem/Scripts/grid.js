﻿var itemNamesArray = [];
var pathArray = [];
var currentFolderID = "00000000-0000-0000-0000-000000000000";
var rootFolderID = "00000000-0000-0000-0000-000000000000";
$(document).ready(function () {
    var currentUrl = window.location.href;
    pathArray[0] = rootFolderID;
    itemNamesArray[0] = 'root'; 
    $("#addFolderbtn" + areaId + "_" + regionId).bind("click", function () {
        var add_folder_window = $("#add_folder_window" + areaId + "_" + regionId).data("kendoWindow");
        $(".k-upload-files.k-reset").remove();
        $(".k-upload-status.k-upload-status-total").remove();
        $("#folderUserMultiSelect" + areaId + "_" + regionId).data("kendoMultiSelect").value("");
        $("#folderUserMultiSelect" + areaId + "_" + regionId).data("kendoMultiSelect").enable(true);
        $('#folderDescrTxb' + areaId + "_" + regionId).val("");
        $('#folderNameTextbox' + areaId + "_" + regionId).val("");
        $("#folderIsShareForAllChkb" + areaId + "_" + regionId).attr('checked', false);
        add_folder_window.bind('activate', function () {
            $('#folderNameTextbox' + areaId + "_" + regionId).focus();
        });
        add_folder_window.center().open();
    });
    $("#grid_" + areaId + "_" + regionId).on("click", "#generated-link" + areaId + "_" + regionId, function (e) {
        var fileId = $(this).data('value');
        var grid = $("#grid_" + areaId + "_" + regionId).data("kendoGrid");
        grid.dataSource.read({ fileSystemId: fileId });
        for (var i = pathArray.length - 1; i >= 0; i--) {
            if (pathArray[i] != fileId) {
                pathArray.pop();
                itemNamesArray.pop();
            } else break;
        }
        ////generate path
        $('#nav-bar' + areaId + "_" + regionId).empty();
        $('#nav-bar' + areaId + "_" + regionId).append('<span> ~ </span><a class="nav-link" id="navToRootFolder' + areaId + "_" + regionId + '">root</a><span> > </span>');
        if (pathArray.length > 1) {
            for (var i = 1; i < pathArray.length; i++) {
                $('#nav-bar' + areaId + "_" + regionId).append('<a  id="generated-link' + areaId + "_" + regionId + '" class="nav-link"  data-value="' + pathArray[i] + '" >' + itemNamesArray[i] + '</a><span> > </span>');
            }
        }
    });
    $("#grid_" + areaId + "_" + regionId).on("click", "#navToRootFolder" + areaId + "_" + regionId, function (e) {
        var grid = $("#grid_" + areaId + "_" + regionId).data("kendoGrid");
        $('#nav-bar' + areaId + "_" + regionId).empty();
        $('#nav-bar' + areaId + "_" + regionId).append('<span> ~ </span><a class="nav-link" id="navToRootFolder' + areaId + "_" + regionId + '">root</a><span> > </span>');
        grid.dataSource.read({ fileSystemId: rootFolderID });
        pathArray = [];
        pathArray[0] = rootFolderID;
        itemNamesArray = [];
        itemNamesArray[0] = 'root';
    });
    $("#grid_" + areaId + "_" + regionId).on("click", "#deleteItem" + areaId + "_" + regionId, function (e) {
        var grid = $("#grid_" + areaId + "_" + regionId).data("kendoGrid");
        var item = grid.dataItem($(this).closest("tr")).FileSystemItemID;
        $("#fileId" + areaId + "_" + regionId).val(item);
        $("#deleteDialog" + areaId + "_" + regionId).data("kendoWindow").center().open();
    });
    $("#grid_" + areaId + "_" + regionId).on("click", "#editItem" + areaId + "_" + regionId, function (e) {
        var grid = $("#grid_" + areaId + "_" + regionId).data("kendoGrid");
        var kendoWindow = $("#edit_window" + areaId + "_" + regionId).kendoWindow({
            title: "Edit",
            modal: true,
            width: 500
        });
        var item = grid.dataItem($(this).closest("tr"));
        $.get(currentUrl + "/Grid/GetEditPartial?itemId=" + item.FileSystemItemID, function (data) {
            kendoWindow.data("kendoWindow").content(data).center().open();
        });
    });
    $("#upload" + areaId + "_" + regionId).bind("click", function () {
        var upload_window = $("#upload_window" + areaId + "_" + regionId).data("kendoWindow");
        //clearing all upload window data
        $(".k-upload-files.k-reset").remove();
        $(".k-upload-status.k-upload-status-total").remove();
        $("#userMultiSelect" + areaId + "_" + regionId).data("kendoMultiSelect").value("");
        $("#userMultiSelect" + areaId + "_" + regionId).data("kendoMultiSelect").enable(true);
        $('#descriptionTextbox' + areaId + "_" + regionId).val("");
        $('#itemNameTextbox' + areaId + "_" + regionId).val("");
        $("#isShareForAllChkb" + areaId + "_" + regionId).attr('checked', false);
        upload_window.center().open();
    });
});
function onUploadError(e) {
    console.log(e.XMLHttpRequest.responseText);
};

function onDataBound() {  

    if (!$("#files" + areaId + "_" + regionId).data("kendoUpload")) {
        $("#files" + areaId + "_" + regionId).kendoUpload({
            async: {
                saveUrl: currentUrl + "/File/UploadFiles"
            },
            complete: onUploadComplete,
            error: onUploadError
        });
    }

    dataView = this.dataSource.view();

    if (dataView.length == 0) {
      
        var header = $("#grid_" + areaId + "_" + regionId + " th.k-header");
        header.each(function (index) {
            var position = $(this).offset();
            var width = $(this).eq(0).outerWidth();
            var height = $(this).eq(0).outerHeight();
            $("<div class='cover'>").css({
                "width": width,
                "height": height,
                "position": "absolute",
                "top": position.top,
                "left": position.left
            }).appendTo($("body"));
        });              
    }
    else {
        $(".cover").remove();       
    }
   

    for (var i = 0; i < dataView.length; i++) {
        if (dataView[i].TypeOfSystemItemID == 2) {
            var uid = dataView[i].uid;
            $("#grid_" + areaId + "_" + regionId + " tbody").find("tr[data-uid=" + uid + "]").addClass(" k-file-item");
        } else {
            var uid = dataView[i].uid;
            $("#grid_" + areaId + "_" + regionId + " tbody").find("tr[data-uid=" + uid + "]").addClass(" k-folder-item");
        }
    }
    $('.typeOfFileItem1' + areaId + "_" + regionId).click(function () {
        var id = $(this).data('value');
        var name = $(this).data('name');
        var grid = $("#grid_" + areaId + "_" + regionId).data("kendoGrid");
        grid.dataSource.read({ fileSystemId: id });
        currentFolderID = id;
        $('#nav-bar' + areaId + "_" + regionId).append('<a id="generated-link' + areaId + "_" + regionId + '" class="nav-link" data-value="' + currentFolderID + '" >' + name + '</a><span> > </span>');
        pathArray.push(currentFolderID);
        itemNamesArray.push(name);
    });
    $('.typeOfFileItem2' + areaId + "_" + regionId).click(function () {
        var grid = $("#grid_" + areaId + "_" + regionId).data("kendoGrid");
        var fileId = $(this).data('value');
        window.location = currentUrl + '/File/Download?id=' + fileId;
    });

};
function onUploadComplete() {
    var filename = $(".k-filename").attr('title');
    var fileNameWithoutExtension = removeExtension(filename);
    $('#itemNameTextbox' + areaId + "_" + regionId).val(fileNameWithoutExtension);
};
function removeExtension(filename) {
    var lastDotPosition = filename.lastIndexOf(".");
    if (lastDotPosition === -1) return filename;
    else return filename.substr(0, lastDotPosition);
}
function cancelDelete() {
    $("#deleteDialog" + areaId + "_" + regionId).data("kendoWindow").close();
};
function submitDelete() {
    var id = $("#fileId" + areaId + "_" + regionId).val();
    $.get(currentUrl + "/File/Delete?itemId=" + id, function (data) {
        var grid = $("#grid_" + areaId + "_" + regionId).data("kendoGrid").dataSource.read({ fileSystemId: pathArray[pathArray.length - 1] });
        $("#deleteDialog" + areaId + "_" + regionId).data("kendoWindow").close();
    });
};
function onBackClick() {
    if (pathArray.length > 1) {
        var grid = $("#grid_" + areaId + "_" + regionId).data("kendoGrid");
        grid.dataSource.read({ fileSystemId: pathArray[pathArray.length - 2] });
        pathArray.pop();
        itemNamesArray.pop();
        ////generate path
        $('#nav-bar' + areaId + "_" + regionId).empty();
        $('#nav-bar' + areaId + "_" + regionId).append('<span> ~ </span><a class="nav-link" id="navToRootFolder' + areaId + "_" + regionId + '">root</a><span> > </span>');
        if (pathArray.length > 1) {
            for (var i = 1; i < pathArray.length; i++) {
                $('#nav-bar' + areaId + "_" + regionId).append('<a  id="generated-link' + areaId + "_" + regionId + '" class="nav-link"  data-value="' + pathArray[i] + '" >' + itemNamesArray[i] + '</a><span> > </span>');
            }
        }
    }
};
function submitAddFolder() {
    var folderName = document.getElementById('folderNameTextbox' + areaId + "_" + regionId).value;
    var description = document.getElementById('folderDescrTxb' + areaId + "_" + regionId).value;
    var usersFoSharing = $("#folderUserMultiSelect" + areaId + "_" + regionId).data("kendoMultiSelect").value();
    var issharedForAll = $("#folderIsShareForAllChkb" + areaId + "_" + regionId).is(':checked');
    if (folderName.length == 0) {
        var notification = $("#popupNotification" + areaId + "_" + regionId).data("kendoNotification");
        notification.show({
            title: "Folder name can not be empty",
            message: "Please enter the folder name "
        }, "error");
    }
    else {
        currentFolderID = pathArray[pathArray.length - 1];
        $.ajax({
            url: currentUrl + "/File/AddFolder",
            type: "POST",
            data: JSON.stringify({
                'parentFolder': currentFolderID, 'folderName': folderName, 'folderDescription': description, "isSharedAll": issharedForAll,
                "usersToShare": usersFoSharing, "areaId": areaId, "regionId": regionId
            }),
            dataType: "json",
            traditional: true,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                var window = $("#add_folder_window" + areaId + "_" + regionId).data("kendoWindow").close();
                var grid = $("#grid_" + areaId + "_" + regionId).data("kendoGrid").dataSource.read({ fileSystemId: currentFolderID });
                //clear data
                $('#folderNameTextbox' + areaId + "_" + regionId).val("");
                $("#folderUserMultiSelect" + areaId + "_" + regionId).data("kendoMultiSelect").value("");
                $('#folderDescrTxb' + areaId + "_" + regionId).val("");
                $("#folderIsShareForAllChkb" + areaId + "_" + regionId).attr('checked', false);
            }
        });
    }
};
function submitUploadFile() {
    var notification = $("#popupNotification" + areaId + "_" + regionId).data("kendoNotification");
    var upload = $("#files" + areaId + "_" + regionId).data("kendoUpload");
    var len = upload.wrapper.find(".k-file").length;
    var description = $("#descriptionTextbox" + areaId + "_" + regionId).val();
    var fileName = $("#itemNameTextbox" + areaId + "_" + regionId).val();
    var usersFoSharing = $("#userMultiSelect" + areaId + "_" + regionId).data("kendoMultiSelect").value();
    var issharedForAll = $("#isShareForAllChkb" + areaId + "_" + regionId).is(':checked');
    if (len === 0) {
        notification.show({
            title: "File not found",
            message: "Please select the file"
        }, "error");
    }
    else if (fileName.length == 0) {
        notification.show({
            title: "File name can not be empty",
            message: "Please enter the file name "
        }, "error");
    }
    else {
        currentFolderID = pathArray[pathArray.length - 1];
        $.ajax({
            url: currentUrl + "/File/SaveToDatabase",
            type: "POST",
            data: JSON.stringify({
                'FileName': fileName, 'description': description, 'parentFolder': currentFolderID, 'isShared': issharedForAll,
                'shareUsers': usersFoSharing, "areaId": areaId, "regionId": regionId
            }),
            dataType: "json",
            traditional: true,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.status == "successfully") {
                    notification.show({
                        title: "Upload successful",
                        message: "The file: " + data.target + " was succesfully added to queue"
                    }, "upload-success");
                }
                else {
                    notification.show({
                        title: "Upload " + data.target + " was failed",
                        message: "Error: " + data.status
                    }, "error");
                }
                var upload_window = $("#upload_window" + areaId + "_" + regionId).data("kendoWindow").close();
                var grid = $("#grid_" + areaId + "_" + regionId).data("kendoGrid").dataSource.read({ fileSystemId: currentFolderID });
            }
        });
    }
};
$("#grid_" + areaId + "_" + regionId).on("mouseenter", ".k-folder-item", function (e) {
    var grid = $("#grid_" + areaId + "_" + regionId).data("kendoGrid");
    var ownerName = grid.dataItem($(this).closest("tr")).OwnerName;
    if (ownerName == fileOwnerName) {       
        $(e.currentTarget).find(".ncRight").css("display", "inline-block");
    }
}).on("mouseleave", ".k-folder-item", function (e) {
    $(e.currentTarget).find(".ncRight").css("display", "none");
});
$("#grid_" + areaId + "_" + regionId).on("mouseenter", ".k-file-item", function (e) {
    var grid = $("#grid_" + areaId + "_" + regionId).data("kendoGrid");
    var ownerName = grid.dataItem($(this).closest("tr")).OwnerName;
    if (ownerName == fileOwnerName) {     
        $(e.currentTarget).find(".ncRight").css("display", "inline-block");
    }
}).on("mouseleave", ".k-file-item", function (e) {
    $(e.currentTarget).find(".ncRight").css("display", "none");
});