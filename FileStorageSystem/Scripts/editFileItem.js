﻿$(document).ready(function () {   
    var currentUrl = window.location.href;
    var itemId=$("#fsitem" + areaIndx + "_" +regionIndx).val();
    if (typeOfItem == 1) {        
        $(".edit-upload").css("display", "none");}
    $.get(currentUrl+"/File/GetSharedUser?fileId="+itemId,function(data){
        $("#editUsersMultiSelect" + areaIndx + "_" +regionIndx).data("kendoMultiSelect").value(data);
    });    
    $("#cancelEdit" + areaIndx + "_" +regionIndx).bind("click", function (e) {
        e.preventDefault();
        $("#edit_window" + areaIndx + "_" +regionIndx).data("kendoWindow").close();
    });
    if (!$("#files"+ areaIndx + "_" +regionIndx+"_").data("kendoUpload")) {
        $("#files"+ areaIndx + "_" +regionIndx+"_").kendoUpload({
            async:{
                saveUrl: currentUrl+"/File/UploadFiles"
            }
        });}});
$.fn.serializeObject = function () {
    var o = {};  
    $(this).find('input[type="hidden"], input[type="text"], input[type="password"], input[type="checkbox"]:checked, input[type="radio"]:checked, select').each(function () {
        if ($(this).attr('type') == 'hidden') { //if checkbox is checked do not take the hidden field
            var $parent = $(this).parent();
            var $chb = $parent.find('input[type="checkbox"][name="' + this.name.replace(/\[/g, '\[').replace(/\]/g, '\]') + '"]');
            if ($chb != null) {
                if ($chb.prop('checked')) return;
            }
        }
        if (this.name === null || this.name === undefined || this.name === '') return;
        var elemValue = null;
        if ($(this).is('select')) elemValue = $(this).find('option:selected').val();
        else elemValue = this.value;
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(elemValue || '');
        } else {
            o[this.name] = elemValue || '';
        }
    });
    return o;};
$("#SubmitEdit" + areaIndx + "_" +regionIndx).bind("click", function (e) {
    e.preventDefault();    
    var di = $("#editForm"+areaIndx + "_" +regionIndx).serializeObject();
    di.ItemName = $('#ItemName'+areaIndx + "_" +regionIndx).val();
    di.ItemDescription=$('#ItemDescription'+areaIndx + "_" +regionIndx).val();
    var usersFoSharing=$("#editUsersMultiSelect" + areaIndx + "_" +regionIndx).data("kendoMultiSelect").value();
    $.ajax({
        url: currentUrl+"/File/EditItem",
        type: "POST",
        data: JSON.stringify({'item': di, 'users':usersFoSharing}),
        dataType: "json",
        traditional: true,
        contentType: "application/json; charset=utf-8",
        success: function () {            
            $("#edit_window" + areaIndx + "_" + regionIndx).data("kendoWindow").close();            
            var grid = $("#grid_" + areaIndx + "_" + regionIndx).data("kendoGrid").dataSource.read({ fileSystemId: parFolderId });
        }
    });});