﻿
function getUrl() {
    var url = window.location.href;
    var currentUrl = url.slice(-1) == "/" ? url : (url + "/");
    return currentUrl;
}

$(document).ready(function () {
    var isUserExist = "false";    

    $.ajax({
        url: getUrl() + "Home/CheckUser",
        type: "POST",
        data: JSON.stringify({ "userName": userName }),
        dataType: "json",
        traditional: true,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data === "true") {
                $("#failUserAccount").css("display", "none");
                $("#headerUserName").text("Hello, "+userName);               
            }
            else {
                var window = $("#failUserAccount").kendoWindow({
                    width: "600px",
                    title: "Error",
                    actions: {}
                });
                window.data("kendoWindow").content(kendo.template($("#failUserTemplate").html())).center().open();

                $(".body-header").css("display", "none");
                $(".container").css("display", "none");
                $("#footer").css("display", "none");
            }
        }
    });


    var tooltip = $("#avatarToolTip").kendoTooltip({
        position: "bottom",
        filter: "img",
        width: 220,
        content: "Click on image to change avatar"
    }).data("kendoTooltip");
    $("#uploadImage").kendoUpload({
        async: {
            saveUrl: getUrl() + "Avatar/Upload"
        },
        multiple: false,
        success: onUploadImageSuccess
    });


});
function onDashboardOpen() {
    var dashboardTemplate = kendo.template($("#dashboardTemplate").html());
    var window = $("#dashboard_window").kendoWindow({
        title: "Dashboard",
        modal: true,
        draggable: false,
        actions: ["Close"]
    });
    window.data("kendoWindow").content(dashboardTemplate).center().maximize().open();
    $("#dashboard-tabstrip").kendoTabStrip({
        animation: false,
        width: "100%"
    });
    $("#dashboard-tabstrip").data("kendoTabStrip").select(0);
};
function welcomKitOpen() {
    var win = window.open(welcomeKitLink, '_blank');
    win.focus();

};