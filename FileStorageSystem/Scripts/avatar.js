﻿var jcrop_api,
    boundx,
    boundy,
    xsize,
    ysize,
    currImage;

function getUrl() {
    var url = window.location.href;
    var currentUrl = url.slice(-1) == "/" ? url : (url + "/");
    return currentUrl;
}

function changeUserIcon() {
    $("#uploadImage").click();
};
function saveAvatar() {
    var img = $('#preview-pane .preview-container img');
    $.ajax({
        type: "GET",
        url: getUrl() + "Avatar/Save",
        traditional: true,       
        data: {
            w: img.css('width'),
            h: img.css('height'),
            l: img.css('marginLeft'),
            t: img.css('marginTop'),
            fileName: img.attr('src')
        },
        success: function (data) {                  
            $("#userAccIcon").attr('src', avatarPath + '?' + new Date().getTime());
            $("#avatarUploadWindow").data("kendoWindow").close();
        },
        error: function (e) {
            alert('Cannot upload avatar at this time ' + e.errorMessage);
        }
    });   
};
function onUploadImageSuccess(e) {
    if (e.response.success === false) {
        var notification = $("#popupNotification1_1").data("kendoNotification");
        notification.show({
            title: "Error",
            message: e.response.errorMessage
        }, "error");
    }
    else {
        var img = $('#crop-avatar-target');
        if ($('#crop-avatar-target').data('Jcrop')) {
            $('#crop-avatar-target').data('Jcrop').destroy();
            jcrop_api = null;
            $('#crop-avatar-target').removeAttr('style');
        }
        currImage = getUrl().slice(0,-1) + e.response.fileName;       
        $('.jcrop-holder img').attr('src', currImage);
        $('#preview-pane .preview-container img').attr('src', currImage);
        img.attr('src', currImage);
        img.Jcrop({
            boxWidth: 650,
            boxHeight: 600,
            onChange: updatePreviewPane,
            onSelect: updatePreviewPane,
            aspectRatio: xsize / ysize
        }, function () {
            var bounds = this.getBounds();
            boundx = bounds[0];
            boundy = bounds[1];

            jcrop_api = this;
            jcrop_api.setOptions({ allowSelect: true });
            jcrop_api.setOptions({ allowMove: true });
            jcrop_api.setOptions({ allowResize: true });
            jcrop_api.setOptions({ aspectRatio: 1 });
            var padding = 10;
            var shortEdge = (boundx < boundy ? boundx : boundy) - padding;
            var longEdge = boundx < boundy ? boundy : boundx;
            var xCoord = longEdge / 2 - shortEdge / 2;
            jcrop_api.animateTo([xCoord, padding, shortEdge, shortEdge]);
            var p = "<div id='preview-pane'><div class='preview-container'> <img class='jcrop-preview' src='" + currImage + "' /></div></div>";;
            $(p).appendTo(jcrop_api.ui.holder);
            var pcnt = $('#preview-pane .preview-container');
            xsize = pcnt.width();
            ysize = pcnt.height();
            jcrop_api.focus();
            var bigImgWidth = $(".jcrop-holder img")[0].width;
            var bigImgHeight = $(".jcrop-holder img")[0].height;
            var window = $("#avatarUploadWindow").data("kendoWindow");
            window.wrapper.css({
                width: (bigImgWidth + 150)
            });
            window.center().open();
        });
    }
};
function updatePreviewPane(c) {
    if (parseInt(c.w) > 0) {
        var rx = xsize / c.w;
        var ry = ysize / c.h;
        $('#preview-pane .preview-container img').css({
            width: Math.round(rx * boundx) + 'px',
            height: Math.round(ry * boundy) + 'px',
            marginLeft: '-' + Math.round(rx * c.x) + 'px',
            marginTop: '-' + Math.round(ry * c.y) + 'px'
        });
    }
};
