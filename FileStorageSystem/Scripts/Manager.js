﻿var currItemStatus = "";
function getCurrentUrl() {    
    return window.location.href;
}
$(document).ready(function () { 
    $("#grid").on("click", "#btnDownload", function (e) {
        var fileId = $(this).data('value');       
        window.location = getCurrentUrl() + '/Manage/Download?id=' + fileId;
    });
    $("#grid").on("click", ".approve-button", function (e) {
        var fileId = $(this).data('value');
        $.ajax({
            url: getCurrentUrl() + "/Manage/ApproveItem",
            type: "POST",
            data: JSON.stringify({ "id": fileId }),
            dataType: "json",
            traditional: true,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                var grid = $("#grid").data("kendoGrid").dataSource.read({ itemStatus: currItemStatus });
            }
        });
    });
    $("#grid").on("click", ".reject-button", function (e) {
        var fileId = $(this).data('value');
        $.ajax({
            url: getCurrentUrl() + "/Manage/RejectItem",
            type: "POST",
            data: JSON.stringify({ "id": fileId }),
            dataType: "json",
            traditional: true,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                var grid = $("#grid").data("kendoGrid").dataSource.read({ itemStatus: currItemStatus });
            }
        });
    });
    $("#grid").on("click", "#deleteButton", function (e) {
        var fileId = $(this).data('value');
        $.ajax({
            url: getCurrentUrl() + "/Manage/DeleteItem",
            type: "POST",
            data: JSON.stringify({ "itemId": fileId }),
            dataType: "json",
            traditional: true,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                var grid = $("#grid").data("kendoGrid").dataSource.read({ itemStatus: currItemStatus });
            }
        });
    });
    currentFolderID = "00000000-0000-0000-0000-000000000000";
});
function onDataBound() {
    //$('#grid table tr').dblclick(function () {
    //    var grid = $("#grid").data("kendoGrid");
    //    grid.dataSource.read({ fileSystemId: grid.dataItem($(this)).FileSystemItemID });
    //    currentFolderID = grid.dataItem($(this)).FileSystemItemID;
    //})
};
function allFilesButtonClick() {
    var grid = $("#grid").data("kendoGrid");
    currItemStatus = "";
    grid.dataSource.read({ itemStatus: currItemStatus });
}
function pendingFilesButtonClick() {
    var grid = $("#grid").data("kendoGrid");
    currItemStatus = 0;
    grid.dataSource.read({ itemStatus: currItemStatus });
}
function approvedFilesButtonClick() {
    var grid = $("#grid").data("kendoGrid");
    currItemStatus = 1;
    grid.dataSource.read({ itemStatus: currItemStatus });
}