﻿function getManagerUrl() {
    var currentUrl = window.location.href;
    var urlItems = [];
    urlItems = currentUrl.split('/');
    var temp = urlItems.slice(0, -3);
    var temp2 = temp.slice(2);
    var managerUrl = 'http://';
    for (var i = 0; i < temp2.length; i++) {
        managerUrl += temp2[i] + '/'; }
    return managerUrl.slice(0, -1);};
function onApprove() {   
		var fileItemId = document.getElementById('itemId').value;
		$.ajax({
		    url: getManagerUrl() + "/Manage/ApproveItem",
			type: "POST",
			data: JSON.stringify({ "id": fileItemId }),
			dataType: "json",
			traditional: true,
			contentType: "application/json; charset=utf-8",
			success: function (data) {
			    window.location = getManagerUrl();}
		});};
function onReject() {
		var fileItemId = document.getElementById('itemId').value;
		$.ajax({
		    url: getManagerUrl() + "/Manage/RejectItem",
			type: "POST",
			data: JSON.stringify({ "id": fileItemId }),
			dataType: "json",
			traditional: true,
			contentType: "application/json; charset=utf-8",
			success: function (data) {
			    window.location = getManagerUrl();
			}
		}); };
function onDelete() {
		var fileItemId = document.getElementById('itemId').value;
		$.ajax({
		    url: getManagerUrl() + "/Manage/DeleteItem",
			type: "POST",
			data: JSON.stringify({ "itemId": fileItemId }),
			dataType: "json",
			traditional: true,
			contentType: "application/json; charset=utf-8",
			success: function (data) {
			    window.location = getManagerUrl();
			}
		});};
function onDownload() {
    console.log(getManagerUrl());
		var fileItemId = document.getElementById('itemId').value;
		window.location = getManagerUrl() + '/Manage/Download?id=' + fileItemId;
	};
