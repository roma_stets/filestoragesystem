﻿function getUrl() {
    var url = window.location.href;
    var currentUrl = url.slice(-1) == "/" ? url : (url + "/");
    return currentUrl;
}

$(document).ready(function () {
    var regions = [];
    var lastRegTabIndex = 0;
    var rootFolderID = "00000000-0000-0000-0000-000000000000";

  $(".header-center-nav").click(function () {
      lastRegTabIndex = 0;
      tabstrip.select(0);    
  });
    
    $.get(getUrl() + "Home/GetRegions", function (data) {
        $.each(data, function (index, value) {
            regions.push({
                text: value.text,
                content: value.content
            });
        });
        $.get(getUrl() + "Home/GetAreas", function (data) {
            $.each(data, function (index, value) {
                tabstrip.append({
                    text: value.text,
                    content: value.content,
                    imageUrl: getUrl() + value.imageUrl
                });
            });
            var tabs = $("div[class$=_Tab]").each(function () {
                $(this).kendoTabStrip({
                    animation: false,
                    select: onSelectRegion,
                    dataTextField: "text",
                    dataContentField: "content",
                    dataSource: regions
                }).data("kendoTabStrip");
            });
            tabstrip.select(0);
        });
    });
    var tabstrip = $("#tabstrip").kendoTabStrip({
        animation: false,
        dataImageUrlField: "imageUrl",
        select: onSelectArea,
    }).data("kendoTabStrip");
    $(".k-tabstrip-items.k-reset").first().addClass("area-Tab-Ul");
    function onSelectRegion(e) {       
        var areaTabText = $("#tabstrip").kendoTabStrip().data("kendoTabStrip").select().text();
        var areaTabIndex = $("#tabstrip").kendoTabStrip().data("kendoTabStrip").select().index();
        var regTabName = $(e.item).find("> .k-link").text();
        var regTabIndex = $(e.item).select().index();
        var len = document.getElementById("grid_" + (areaTabIndex + 1) + "_" + (regTabIndex + 1));
        var tabInst = this;
        lastRegTabIndex = regTabIndex;
        $(e.contentElement).css("opacity", "1");
        areaId = (areaTabIndex + 1);
        regionId = (regTabIndex + 1);
        if (len == null) {          
            $.get(getUrl()+ "Grid/GetGridView?areaName=" + areaTabText + "&regionName=" + regTabName, function (data) {
                $(tabInst.contentElement(regTabIndex)).append(data);
            });
        }
        else {
            var grid = $("#grid_" + (areaTabIndex + 1) + "_" + (regTabIndex + 1)).data("kendoGrid");
            grid.dataSource.read({ fileSystemId: rootFolderID });
            $('#nav-bar' + (areaTabIndex + 1) + "_" + (regTabIndex + 1)).empty();
            $('#nav-bar' + (areaTabIndex + 1) + "_" + (regTabIndex + 1)).append('<span> ~ </span><a class="nav-link" id="navToRootFolder' + (areaTabIndex + 1) + "_" + (regTabIndex + 1) + '">root</a><span> > </span>');
            pathArray = [];
            pathArray[0] = rootFolderID;
            itemNamesArray = [];
            itemNamesArray[0] = 'root';
        }
    };
    function onSelectArea(e) {
        var areaTabName = $(e.item).find("> .k-link").text();
        var areaIndex = $(e.item).select().index();
        var inTab = $("." + areaTabName + "_Tab").kendoTabStrip().data("kendoTabStrip");
        inTab.select(lastRegTabIndex);
        var len = document.getElementById("grid_" + (areaIndex + 1) + "_" + (lastRegTabIndex + 1));
        areaId = (areaIndex + 1);
        regionId = (lastRegTabIndex + 1);
        if (len == null) {            
            $.get(getUrl()+ "Grid/GetGridView?areaName=" + areaTabName + "&regionName=" + regions[lastRegTabIndex].text, function (data) {
                $(inTab.contentElement(lastRegTabIndex)).append(data);
            });
        }
        else {
            var grid = $("#grid_" + (areaIndex + 1) + "_" + (lastRegTabIndex + 1)).data("kendoGrid");
            grid.dataSource.read({ fileSystemId: rootFolderID });
            $('#nav-bar' + (areaIndex + 1) + "_" + (lastRegTabIndex + 1)).empty();
            $('#nav-bar' + (areaIndex + 1) + "_" + (lastRegTabIndex + 1)).append('<span> ~ </span><a class="nav-link" id="navToRootFolder' + (areaIndex + 1) + "_" + (lastRegTabIndex + 1) + '">root</a><span> > </span>');
            pathArray = [];
            pathArray[0] = rootFolderID;
            itemNamesArray = [];
            itemNamesArray[0] = 'root';
        }
    };
});