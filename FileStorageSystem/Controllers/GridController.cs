﻿using FileStorageSystem.Models;
using FileStorageSystem.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FileStorageSystem.Controllers
{
    public class GridController : Controller
    {

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        // GET: Grid
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult GetGridView(string areaName, string regionName)
        {           
            FileStorageSystemContext context = new FileStorageSystemContext();

            int areaIndex = context.Areas.FirstOrDefault(a => a.AreaName == areaName).AreaId;
            int regionIndex = context.Regions.FirstOrDefault(r => r.RegionName == regionName).RegionId;
            TabNavViewModel model = new TabNavViewModel()
            {
                AreaIndex = areaIndex,
                RegionIndex = regionIndex
            };

            return PartialView( "~/Views/Grid/Grid.cshtml", model);
        }

        public ActionResult Get_items(string fileSystemId, [DataSourceRequest] DataSourceRequest request, int areaId, int regionId)
        {          
            FileStorageSystemContext context = new FileStorageSystemContext();
            List<FileItemViewModel> files = new List<FileItemViewModel>();
            Guid fileId;
            Guid.TryParse(fileSystemId, out fileId);

            string userName = this.User.Identity.Name.Split('\\')[1];

            if (context.Users.Where(u=>u.UserName==userName).Count() == 0)
            {
                return Content("");
            }

            Guid userId = context.Users.Where(u => u.UserName == userName).FirstOrDefault().UserID;

            var items = from storageItem in context.FileSystemItems
                        where storageItem.ParentFolderID == fileId && storageItem.isApproved == 1
                                        && storageItem.AreaId == areaId && storageItem.RegionId == regionId
                                        && storageItem.OwnerID == userId
                        select storageItem;

            List<FileSystemItem> allFiles = items.ToList();

            GetSharedFiles(fileId, userId, areaId, regionId, ref allFiles);
            GetSharedToAllFiles(fileId, areaId, regionId, userId, ref allFiles);

            var res = allFiles.GroupBy(i => i.FileSystemItemID, (key, group) => group.First());

            foreach (var item in res)
            {
                files.Add(new FileItemViewModel()
                {
                    FileSystemItemID = item.FileSystemItemID,
                    CreatingDate = item.CreatingDate,
                    isApproved = item.isApproved,
                    IsSharedToAll = item.IsSharedToAll,
                    ItemDescription = item.ItemDescription,
                    ItemName = item.ItemName,
                    ParentFolderID = item.ParentFolderID,
                    TypeOfSystemItemID = item.TypeOfSystemItemID,
                    OwnerName = context.Users.First(u => u.UserID == item.OwnerID).UserName,
                    FileExtensionId = item.FileExtensionId,
                    AreaId = item.AreaId,
                    RegionId = item.RegionId
                });
            }
          
            return Json(files.ToDataSourceResult(request));
        }

        private void GetSharedFiles(Guid fileId, Guid userId, int areaId, int regionId, ref List<FileSystemItem> allFiles)
        {
            List<FileSystemItem> result = new List<FileSystemItem>();
            FileStorageSystemContext context = new FileStorageSystemContext();

            var sharedItems = from item in context.SharedFiles.ToList()
                              where item.UserId == userId
                              select item;
            if (sharedItems != null)
            {
                if (fileId == Guid.Empty)
                {
                    foreach (var item in sharedItems)
                    {
                        FileSystemItem temp = context.FileSystemItems.Where(i => i.FileSystemItemID == item.FileId).FirstOrDefault();
                        if (temp.AreaId == areaId && temp.RegionId == regionId && temp.isApproved == 1)
                        {
                            allFiles.Add(temp);
                        }
                    }
                }
                else
                {
                    var temps = context.FileSystemItems.Where(c => c.ParentFolderID == fileId).ToList();
                    if (temps != null)
                    {
                        foreach (var item in temps)
                        {
                            if (item.isApproved == 1)
                            {
                                allFiles.Add(item);
                            }
                        }
                    }
                }
            }

        }
        private void GetSharedToAllFiles(Guid fileId, int areaId, int regionId, Guid userId, ref List<FileSystemItem> allFiles)
        {
            List<FileSystemItem> result = new List<FileSystemItem>();
            FileStorageSystemContext context = new FileStorageSystemContext();

            if (fileId == Guid.Empty)
            {
                var sharedToAllItems = context.FileSystemItems.Where(f => f.IsSharedToAll == 1);
                foreach (var item in sharedToAllItems)
                {
                    if (item.AreaId == areaId && item.RegionId == regionId && item.isApproved == 1 && item.OwnerID != userId)
                    {
                        allFiles.Add(item);
                    }
                }
            }
            else
            {
                var temps = context.FileSystemItems.Where(c => c.ParentFolderID == fileId).ToList();
                if (temps != null)
                {
                    foreach (var item in temps)
                    {
                        if (item.isApproved == 1)
                        {
                            allFiles.Add(item);
                        }
                    }
                }
            }
        }
        public ActionResult GetUsers()
        {
            FileStorageSystemContext context = new FileStorageSystemContext();
            List<UserViewModel> users = new List<UserViewModel>();
            foreach (var user in context.Users.ToList())
            {
                users.Add(new UserViewModel()
                {
                    Email = user.UserEmail,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    UserID = user.UserID,
                    UserName = user.UserName
                });
            }


            return Json(users, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult GetEditPartial(Guid itemId)
        {

            FileStorageSystemContext context = new FileStorageSystemContext();
            FileSystemItem item = context.FileSystemItems.First(i => i.FileSystemItemID == itemId);

            FileItemViewModel model = new FileItemViewModel()
            {
                FileSystemItemID = item.FileSystemItemID,
                CreatingDate = item.CreatingDate,
                isApproved = item.isApproved,
                IsSharedToAll = item.IsSharedToAll,
                ItemDescription = item.ItemDescription,
                ItemName = item.ItemName,
                ParentFolderID = item.ParentFolderID,
                TypeOfSystemItemID = item.TypeOfSystemItemID,
                OwnerName = context.Users.First(u => u.UserID == item.OwnerID).UserName,
                FileExtensionId = item.FileExtensionId,
                AreaId = item.AreaId,
                RegionId = item.RegionId
            };

            return PartialView("~/Views/Shared/EditorTemplates/FileSystemItem.cshtml", model);
        }
    }
}