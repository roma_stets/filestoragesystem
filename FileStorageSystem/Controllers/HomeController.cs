﻿using FileStorageSystem.Models;
using FileStorageSystem.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace FileStorageSystem.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Tab()
        {
            return View();
        }
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        public ActionResult GetFirstUser()
        {
            logger.Info("Hello line 1");
            FileStorageSystemContext context = new FileStorageSystemContext();
            logger.Info("Hello line 2");
            try
            {
                logger.Info("Hello try block");
                User user = context.Users.First();
                logger.Info("user is " + user.UserName);
                string result = user.UserName + " " + user.UserEmail;
                logger.Info("send result to view");
                return PartialView("GetFirstUser", result.ToString());
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString(), ex);
                return PartialView("GetFirstUser", ex.ToString());
            }

        }

        public ActionResult CheckUser(string userName)
        {
            FileStorageSystemContext context = new FileStorageSystemContext();

            if (context.Users.Where(u => u.UserName == userName).Count() != 0)
            {
                return Json("true", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("false", JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult GetAreas()
        {
            FileStorageSystemContext context = new FileStorageSystemContext();
            List<TabStripViewModel> areas = new List<TabStripViewModel>();

            foreach (var item in context.Areas.ToList())
            {
                areas.Add(new TabStripViewModel()
                {
                    text = item.AreaName,
                    content = "<div class='" + item.AreaName + "_Tab'></div>",
                    imageUrl = "Content/Bleudata_icons/areas/" + item.AreaName + ".png"
                });
            }
            return Json(areas, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetRegions()
        {
            FileStorageSystemContext context = new FileStorageSystemContext();
            List<TabStripViewModel> regions = new List<TabStripViewModel>();

            foreach (var item in context.Regions.ToList())
            {
                regions.Add(new TabStripViewModel()
                {
                    text = item.RegionName,
                    content = ""
                }

                     );
            }


            return Json(regions, JsonRequestBehavior.AllowGet);
        }


    }
}
