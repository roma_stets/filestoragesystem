﻿using FileStorageSystem.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace FileStorageSystem.Controllers
{
	public class AvatarController : Controller
	{
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private const int AvatarStoredWidth = 100;  // ToDo - Change the size of the stored avatar image
		private const int AvatarStoredHeight = 100; // ToDo - Change the size of the stored avatar image
		private const int AvatarScreenWidth = 400;  // ToDo - Change the value of the width of the image on the screen
		private const string TempFolder = "/Temp";
		private const string MapTempFolder = "~" + TempFolder;
		private readonly string[] _imageFileExtensions = { ".jpg", ".png", ".gif", ".jpeg" };
	
		public ActionResult Upload(IEnumerable<HttpPostedFileBase> uploadImage)
		{
			if (uploadImage == null || !uploadImage.Any()) return Json(new { success = false, errorMessage = "No file uploaded." });
			var file = uploadImage.FirstOrDefault();  // get ONE only
			if (file == null || !IsImage(file)) return Json(new { success = false, errorMessage = "File is of wrong format." });
			if (file.ContentLength <= 0) return Json(new { success = false, errorMessage = "File cannot be zero length." });
			var webPath = GetTempSavedFilePath(file);
			//mistertommat - 18 Nov '15 - replacing '\' to '//' results in incorrect image url on firefox and IE,
			//                            therefore replacing '\\' to '/' so that a proper web url is returned.            
			var path = webPath.Replace("\\", "/");
			return Json(new { success =true, fileName = path }); // success
		}

		[HttpGet]
		public ActionResult Save(string t, string l, string h, string w, string fileName)
		{
			try
			{
				// Calculate dimensions
				var top = Convert.ToInt32(t.Replace("-", "").Replace("px", ""));
				var left = Convert.ToInt32(l.Replace("-", "").Replace("px", ""));
				var height = Convert.ToInt32(h.Replace("-", "").Replace("px", ""));
				var width = Convert.ToInt32(w.Replace("-", "").Replace("px", ""));

				// Get file from temporary folder
				var fn = Path.Combine(Server.MapPath(MapTempFolder))+"\\"+Path.GetFileName(fileName);
				// ...get image and resize it, ...
				var img = new WebImage(fn);
				img.Resize(width, height);
                // ... crop the part the user selected, ...
                int bottom = img.Height - top - AvatarStoredHeight;
                int top2 = img.Width - left - AvatarStoredWidth;

                img.Crop(top, left, bottom>0?bottom:0, top2>0?top2:0);
				// ... delete the temporary file,...
				System.IO.File.Delete(fn);
				// ... and save the new one.

				FileStorageSystemContext context = new FileStorageSystemContext();
                string userName = this.User.Identity.Name.Split('\\')[1];
                if (userName == "")
                {
                    return Content("");
                }
                User user = context.Users.Where(u => u.UserName == userName).FirstOrDefault();
				user.Avatar = img.GetBytes();
				context.Entry(user).State = System.Data.Entity.EntityState.Modified;
				context.SaveChanges();

				return Json("",JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
                logger.Error(string.Format("type: " + ex.GetType() + "---Target: " + ex.TargetSite + "---Message: " + ex.Message), ex);
                return Json(new { success = false, errorMessage = "Unable to upload file.\nERRORINFO: " + ex.Message });
			}
		}

		private bool IsImage(HttpPostedFileBase file)
		{
			if (file == null) return false;
			return file.ContentType.Contains("image") ||
				 _imageFileExtensions.Any(item => file.FileName.EndsWith(item, StringComparison.OrdinalIgnoreCase));
		}

		private string GetTempSavedFilePath(HttpPostedFileBase file)
		{
            string serverPath = Server.MapPath(MapTempFolder);
           
			if (Directory.Exists(serverPath) == false)
			{
				Directory.CreateDirectory(serverPath);
			}

			// Generate unique file name
			var fileName = Path.GetFileName(file.FileName);
			fileName = SaveTemporaryAvatarFileImage(file, serverPath, fileName);

			// Clean up old files after every save
			CleanUpTempFolder(1);
			return  Path.Combine(TempFolder, fileName);
		}

		private static string SaveTemporaryAvatarFileImage(HttpPostedFileBase file, string serverPath, string fileName)
		{
			var img = new WebImage(file.InputStream);
			var ratio = img.Height / (double)img.Width;
			img.Resize(AvatarScreenWidth, (int)(AvatarScreenWidth * ratio));

			var fullFileName = Path.Combine(serverPath, fileName);
			if (System.IO.File.Exists(fullFileName))
			{
				System.IO.File.Delete(fullFileName);
			}
			img.Save(fullFileName);
			return Path.GetFileName(img.FileName);
		}

		private void CleanUpTempFolder(int hoursOld)
		{
			try
			{
				var currentUtcNow = DateTime.UtcNow;
                string serverPath = Server.MapPath(MapTempFolder);
                if (!Directory.Exists(serverPath)) return;
				var fileEntries = Directory.GetFiles(serverPath);
				foreach (var fileEntry in fileEntries)
				{
					var fileCreationTime = System.IO.File.GetCreationTimeUtc(fileEntry);
					var res = currentUtcNow - fileCreationTime;
					if (res.TotalHours > hoursOld)
					{
						System.IO.File.Delete(fileEntry);
					}
				}
			}
            catch (Exception ex)
            {
                logger.Error(string.Format("type: " + ex.GetType() + "---Target: " + ex.TargetSite + "---Message: " + ex.Message), ex);
            }
        }

		public ActionResult GetUserAvatar()
		{
			FileStorageSystemContext context = new FileStorageSystemContext();
            string userName = this.User.Identity.Name.Split('\\')[1];
            if (context.Users.Where(u=>u.UserName==userName).Count() == 0)
            {
                return Content("");
            }
            byte[] avatar = context.Users.Where(u => u.UserName == userName).FirstOrDefault().Avatar;
            if (avatar == null)
            {
                return Content("");
            }

            return File(avatar, "image/png");
		}

	}
}