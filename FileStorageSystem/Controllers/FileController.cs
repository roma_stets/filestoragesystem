﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FileStorageSystem.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Net;
using System.Net.Mail;
using System.Web.Configuration;
using System.Threading.Tasks;
using FileStorageSystem.ViewModels;
using System.Drawing;
using System.Drawing.Drawing2D;
using FileStorageSystem.Common;

namespace FileStorageSystem.Controllers
{
    public class FileController : Controller
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public ActionResult UploadFiles(IEnumerable<HttpPostedFileBase> files)
        {
            HttpPostedFileBase file = files.First();
            Session["UploadedFile"] = null;
            if (file != null)
            {
                string fileExtension = Path.GetExtension(file.FileName).Replace(".", "");
                Guid fileId = Guid.NewGuid();
                string physicalPath = Path.Combine(Server.MapPath("~/App_Data"), fileId.ToString() + "." + fileExtension);

                Session["UploadedFile"] = new PhysicalFileViewModel()
                {
                    FileExtension = fileExtension,
                    fileId = fileId,
                    PhysicalPath = physicalPath,
                    FileSize = file.ContentLength
                };

                try
                {
                    file.SaveAs(physicalPath);
                }
                catch (Exception ex)
                {
                    logger.Error(string.Format("type: " + ex.GetType() + "---Target: " + ex.TargetSite + "---Message: " + ex.Message), ex);
                }

            }

            return Content("");
        }

        [HttpPost]
        public ActionResult SaveToDatabase(string description, string parentFolder, string isShared, string[] shareUsers, string FileName, int areaId, int regionId)
        {
            FileStorageSystemContext context = new FileStorageSystemContext();
            PhysicalFileViewModel uploadedFile = (PhysicalFileViewModel)Session["UploadedFile"];

            string uploadStatus = string.Empty;
            string userName = this.User.Identity.Name.Split('\\')[1];

            Guid userId = context.Users.Where(u => u.UserName == userName).First().UserID;

            FileSystemItem nItem = new FileSystemItem()
            {
                FileSystemItemID = uploadedFile.fileId,
                CreatingDate = DateTime.Now,
                FileSize = uploadedFile.FileSize,
                ItemName = FileName,
                TypeOfSystemItemID = 2,
                PhysicalPath = uploadedFile.PhysicalPath,
                ItemDescription = description,
                ParentFolderID = Guid.Parse(parentFolder),
                IsSharedToAll = isShared == "True" ? 1 : 0,
                OwnerID = userId,
                isApproved = 0,
                FileExtensionId = uploadedFile.FileExtension,
                AreaId = areaId,
                RegionId = regionId
            };
            context.FileSystemItems.Add(nItem);

            if (shareUsers != null)
            {
                foreach (var user in ConvertAutoCompleteToList(shareUsers))
                {
                    SharedFile shrdf = new SharedFile()
                    {
                        UserId = user.UserID,
                        SharedFileID = Guid.NewGuid(),
                        FileId = nItem.FileSystemItemID
                    };
                    context.SharedFiles.Add(shrdf);
                }
                context.SaveChanges();
            }

            try
            {
                var scresult = context.SaveChanges();
                Task.Factory.StartNew(() => MessageService.SendToAdminsApproveEmail(nItem, GetMainUrl()));
                uploadStatus = "successfully";
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("type: " + ex.GetType() + "---Target: " + ex.TargetSite + "---Message: " + ex.Message), ex);
                uploadStatus = "Something wrong! The file didn't uploaded";
            }

            Session["UploadedFile"] = null;

            return Json(new { status = uploadStatus, target = FileName });
        }

        private string GetMainUrl()
        {
            var tempUrl = Request.Url;
            string url = tempUrl.Authority + string.Join(null, tempUrl.Segments, 0, (tempUrl.Segments.Length - 2));
            return url;
        }


        public ActionResult Delete(Guid itemId)
        {
            string message = string.Empty;

            if (itemId != null)
            {
                int res = DeleteItem(itemId);
                if (res == 1)
                {
                    message = "error";
                }
            }
            return Json(message, JsonRequestBehavior.AllowGet);

        }


        public int DeleteItem(Guid itemId)
        {
            using (FileStorageSystemContext context = new FileStorageSystemContext())
            {
                foreach (SharedFile file in context.SharedFiles.Where(f => f.FileId == itemId).ToList())
                {
                    context.SharedFiles.Remove(file);
                    context.SaveChanges();
                }
            }
            FileSystemItem deletedFile;
            using (FileStorageSystemContext context = new FileStorageSystemContext())
            {
                deletedFile = context.FileSystemItems.Where(x => x.FileSystemItemID == itemId).FirstOrDefault();
                if (deletedFile != null)
                {
                    context.FileSystemItems.Remove(deletedFile);
                    context.SaveChanges();
                }
                else { return 1; }
            }
            if (deletedFile.TypeOfSystemItemID == (int)TypeOfStorageItem.FILE)
            {
                return 0;
            }
            var childItems = from child in new FileStorageSystemContext().FileSystemItems
                             where child.ParentFolderID == itemId
                             select child.FileSystemItemID;

            if (childItems.Count() != 0)
            {
                foreach (var fileItem in childItems)
                {
                    DeleteItem(fileItem);
                }
            }
            return 0;
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditItem(FileItemViewModel item, string[] users)
        {
            if (item != null && ModelState.IsValid)
            {
                FileStorageSystemContext context = new FileStorageSystemContext();
                FileSystemItem editedItem = context.FileSystemItems.First(f => f.FileSystemItemID == item.FileSystemItemID);

                editedItem.CreatingDate = item.CreatingDate;
                editedItem.isApproved = item.isApproved;
                editedItem.IsSharedToAll = item.IsSharedToAll;
                editedItem.ItemName = item.ItemName;
                editedItem.ItemDescription = item.ItemDescription;
                editedItem.TypeOfSystemItemID = item.TypeOfSystemItemID;
                editedItem.ParentFolderID = item.ParentFolderID;
                editedItem.FileSystemItemID = item.FileSystemItemID;
                editedItem.AreaId = item.AreaId;
                editedItem.RegionId = item.RegionId;

                ShareToAll(editedItem, item.IsSharedToAllBool);

                context.Entry(editedItem).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();

                EditShareTo(users, item.FileSystemItemID);

                EditUploadedFile(item.FileSystemItemID);
            }

            return Json("", JsonRequestBehavior.AllowGet);
        }

        public void EditUploadedFile(Guid fileId)
        {
            FileStorageSystemContext context = new FileStorageSystemContext();
            FileSystemItem fileItem = context.FileSystemItems.First(f => f.FileSystemItemID == fileId);

            PhysicalFileViewModel uploadedFile = (PhysicalFileViewModel)Session["UploadedFile"];

            if (uploadedFile != null)
            {
                string userName = this.User.Identity.Name.Split('\\')[1];
                Guid userId = context.Users.Where(u => u.UserName == userName).First().UserID;

                var nItem = new FileSystemItem()
                {
                    FileSystemItemID = uploadedFile.fileId,
                    CreatingDate = DateTime.Now,
                    FileSize = uploadedFile.FileSize,
                    ItemName = fileItem.ItemName,
                    TypeOfSystemItemID = 2,
                    PhysicalPath = uploadedFile.PhysicalPath,
                    ItemDescription = fileItem.ItemDescription,
                    ParentFolderID = fileItem.ParentFolderID,
                    IsSharedToAll = fileItem.IsSharedToAll,
                    OwnerID = userId,
                    isApproved = 0,
                    FileExtensionId = uploadedFile.FileExtension,
                    AreaId = fileItem.AreaId,
                    RegionId = fileItem.RegionId
                };

                List<SharedFile> shared = context.SharedFiles.Where(s => s.FileId == fileItem.FileSystemItemID).ToList();
                List<SharedFile> sharedNew = new List<SharedFile>();

                if (shared.Count() == 0)
                {
                    try
                    {
                        context.FileSystemItems.Remove(fileItem);
                        context.SaveChanges();
                        context.FileSystemItems.Add(nItem);
                        var csresult = context.SaveChanges();
                        Task.Factory.StartNew(() => MessageService.SendToAdminsApproveEmail(nItem, GetMainUrl()));
                    }
                    catch (Exception ex)
                    {
                        logger.Error(string.Format("type: " + ex.GetType() + "---Target: " + ex.TargetSite + "---Message: " + ex.Message), ex);
                    }
                }
                else
                {
                    foreach (var sharedFile in shared)
                    {
                        sharedNew.Add(new SharedFile()
                        {
                            FileId = nItem.FileSystemItemID,
                            UserId = sharedFile.UserId,
                            SharedFileID = Guid.NewGuid()
                        });
                        context.SharedFiles.Remove(sharedFile);
                    }
                    context.SaveChanges();

                    context.FileSystemItems.Remove(fileItem);
                    context.FileSystemItems.Add(nItem);
                    foreach (var item in sharedNew)
                    {
                        context.SharedFiles.Add(item);
                    }
                    var csresult = context.SaveChanges();

                    Task.Factory.StartNew(() => MessageService.SendToAdminsApproveEmail(nItem, GetMainUrl()));

                }
                Session["UploadedFile"] = null;
            }
        }

        public JsonResult AddFolder(string folderName, Guid parentFolder, string folderDescription, bool isSharedAll, string[] usersToShare, int areaId, int regionId)
        {
            FileStorageSystemContext context = new FileStorageSystemContext();
            string userName = this.User.Identity.Name.Split('\\')[1];
            Guid userId = context.Users.Where(u => u.UserName == userName).First().UserID;
            FileSystemItem folder = new FileSystemItem()
            {

                CreatingDate = DateTime.Now,
                FileSystemItemID = Guid.NewGuid(),
                TypeOfSystemItemID = 1,
                ParentFolderID = parentFolder,
                ItemName = folderName,
                ItemDescription = folderDescription,
                IsSharedToAll = isSharedAll == true ? 1 : 0,
                isApproved = 1,
                FileExtensionId = "folder",
                FileSize = 0,
                OwnerID = userId,
                AreaId = areaId,
                RegionId = regionId
            };
            context.FileSystemItems.Add(folder);
            if (usersToShare != null)
            {
                foreach (var user in ConvertAutoCompleteToList(usersToShare))
                {
                    context.SharedFiles.Add(new SharedFile
                    {
                        UserId = user.UserID,
                        SharedFileID = Guid.NewGuid(),
                        FileId = folder.FileSystemItemID
                    });
                }
            }
            context.SaveChanges();

            return Json("");
        }

        public ActionResult Download(Guid id)
        {
            var context = new FileStorageSystemContext();
            var filePath = context.FileSystemItems
                 .Where(e => e.FileSystemItemID.Equals(id))
                 .Select(s => s.PhysicalPath).FirstOrDefault();
            var fileName = context.FileSystemItems
                 .Where(e => e.FileSystemItemID.Equals(id))
                 .Select(s => s.ItemName).FirstOrDefault();
            var fileExtension = context.FileSystemItems
                 .Where(e => e.FileSystemItemID.Equals(id))
                 .Select(s => s.FileExtensionId).FirstOrDefault();

            var fullName = fileName + "." + fileExtension;

            FileInfo file = new FileInfo(filePath);
            try
            {
                DownloadFile.TransmitFile(filePath, fullName, Response);
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("type: " + ex.GetType() + "---Target: " + ex.TargetSite + "---Message: " + ex.Message), ex);
            }


            return Content("");
        }

        private void ShareToAll(FileSystemItem item, bool shareToAll)
        {
            using (FileStorageSystemContext context = new FileStorageSystemContext())
            {
                item.IsSharedToAll = shareToAll == true ? 1 : 0;
                context.Entry(item).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }


        private void ShareToUser(User user, Guid fileItemId)
        {
            using (FileStorageSystemContext context = new FileStorageSystemContext())
            {
                context.SharedFiles.Add(new SharedFile()
                {
                    SharedFileID = Guid.NewGuid(),
                    FileId = fileItemId,
                    UserId = user.UserID
                });

                context.SaveChanges();
            }
        }
        private List<User> ConvertAutoCompleteToList(string[] users)
        {
            FileStorageSystemContext context = new FileStorageSystemContext();
            List<User> usersToShare = new List<User>();
            if (users != null)
            {
                foreach (var userName in users)
                {
                    User srdUser = context.Users.Where(u => u.UserName == userName).FirstOrDefault();
                    if (srdUser != null)
                    {
                        usersToShare.Add(srdUser);
                    }
                }
            }

            return usersToShare;
        }

        public ActionResult GetSharedUser(Guid fileId)
        {
            FileStorageSystemContext context = new FileStorageSystemContext();
            List<string> sharedUsers = new List<string>();

            foreach (var item in context.SharedFiles.Where(f => f.FileId == fileId).ToList())
            {
                sharedUsers.Add(context.Users.First(u => u.UserID == item.UserId).UserName);
            }

            return Json(sharedUsers, JsonRequestBehavior.AllowGet);
        }

        private void EditShareTo(string[] users, Guid fileId)
        {
            DeleteShareUsers(fileId);
            if (users != null)
            {
                foreach (var user in ConvertAutoCompleteToList(users))
                {
                    ShareToUser(user, fileId);
                }
            }

        }

        private void DeleteShareUsers(Guid fileId)
        {
            using (FileStorageSystemContext context = new FileStorageSystemContext())
            {
                IEnumerable<SharedFile> shareFiles = context.SharedFiles.Where(f => f.FileId == fileId);
                if (shareFiles.Count() != 0)
                {
                    foreach (SharedFile shrdFile in shareFiles)
                    {
                        context.SharedFiles.Remove(shrdFile);
                    }
                    context.SaveChanges();
                }
            }
        }
    }
}